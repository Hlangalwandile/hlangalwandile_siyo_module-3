import 'package:flutter/material.dart';

const kPrimaryColor = Color.fromARGB(255, 196, 119, 57);
const kPrimaryLightColor = Color(0xFFF1E6FF);
